---
title: "Exploración de datos espaciales"
subtitle: "Ejercicios prácticos de ESDA (Parte 1)"
author: "Gabriel Gaona"
date: '2020-03-03'
output: 
  html_document: 
    fig_width: 8
    fig_height: 4
    fig_caption: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, message = FALSE)
```

## Análisis exploratorio de datos espaciales (ESDA)

Se trata del análisis exploratorio, ya sea estadístico descriptivo o gráfico, de
las variables en el espacio geográfico. EL ESDA permite rápidamente entender el
comportamiento de los fenómenos y su relación más evidente con otros fenómenos.

Tomemos como ejemplo los datos presentados en el fichero ShapeFile de 
`Sectores_censales_variables.shp`. Haremos uso de las librerías `tidyverse` y `sf`
para hacer una rápida exploración de los datos espaciales de esta capa. Primero
cargaremos los datos a la sesión de R. Visualicemos los datos contenidos en las
variables de *"Porcentaje de Hacnamiento"* (`P_Hacina`) y *"Porcentaje de Población 
indígena"* (`P_Indi`) en el sector censal.

```{r data}
library(tidyverse)
library(sf)
library(deldir)

# Datos de variables de sectores censales
variablesshp <- sf::st_read("../Datos/Sectores_censales_variables.shp", quiet = TRUE)

plot(variablesshp[c('P_Indi', 'P_Hacina')], 
     key.pos = 1, axes = TRUE, 
     key.width = lcm(1.3), key.length = 1.0)
```

Ahora analizaremos las variables independientemente haciendo uso de herramientas
gráficas: `Histograma` y `Q-Q Plots`

## Histograma

El histograma es un gráfico de frecuencias absolutas (conteos) o relativas de una
variable de interés. Se puede obtener una idea preliminar sobre la normalidad de
la variable. Si la distribución de las barras del histograma siguen la forma de 
una curva normal (campana de Gauss), entonces la variable posiblemente pertenezca
a una distribución normal. Veamos que pasa en nuestro caso de análisis:

```{r histogram}
variablesshp %>%
  pivot_longer(cols = c(P_Indi, P_Hacina),
               names_to = "variable",
               values_to = "Percent") %>%
 ggplot(aes(Percent, y = ..ndensity..)) +
  #ggplot(aes(Percent, y = ..count..)) +
  geom_histogram(aes(fill = ..count..)) +
  stat_density(geom = "path") +
  facet_grid(~variable, scales = "free_x")
```

El gráfico también contiene las lineas de densidad scalada al máximo de 1. El gráfico
claramente indica que las variables no tienes una forma de campana de Gauss, por
lo tanto es posible que no pertenezcan a una distribución normal.

```{r barplot}
set.seed(157)
variablesshp %>%
  mutate(cluster = cbind(P_Hacina, P_sin_alca, P_no_acc_p, P_Indi) %>%
           dist() %>%
           hclust("ave") %>%
           cutree(k=5)) %>%
  ggplot(aes(cluster)) +
  geom_bar(aes(fill = cluster), color = NA)
```

## Gráfico de Quantil-Quantil

Una forma más directa de visualizar si una variable sigue una distribución 
específica es a través de un gráfico de Quantil-Quantil, más conocido como 
*"Q-Q Plot"*. Se trata de un gráfico de dispersión, en el cual se compara la 
variable (eje *y*) con la distribución teórica. En nuestro caso nos interesa 
conocer si los datos siguen una distribución normal, entonces compararemos con
los valores teóricos de una distribución normal.

```{r qqplot}
variablesshp %>%
  pivot_longer(cols = c(P_Indi, P_Hacina),
               names_to = "variable",
               values_to = "Percent") %>%
ggplot(aes(sample = Percent)) +
  geom_qq(distribution = stats::qnorm) +
  geom_qq_line(distribution = stats::qnorm) +
  facet_grid(~variable, scales = "free_x")
```

Como se aprecia en el gráfico de Quantil - Quantil, las características de las variables `P_Indi` y `P_Hacina` no pertenecen a una distribución normal. Pero vamos a confirmarlo con el test de normalidad de _Kolmogorov-Smirnov_.

```{r KStest}
ks.test(variablesshp$P_Hacina, "pnorm")
ks.test(variablesshp$P_Indi, "pnorm")
```

Los estadísticos `D` se refieren a qué tan diferente estadísticamente es la muestra, comparada con la distribución normal. El `p-value` cercano a cero indica que el valor `D` puede ser aceptado como correcto. Si `D` se aleja de Cero, entonces decimos que las dos series comparadas (Variable vs distribución normal) tienen características estadísticas diferentes. Por lo que podemos estar seguros que los datos no pertenecen a una distribución población con normal.

## Box plot

El box plot es un gráfico de estadísticas descriptivas de una variable. No es
requisito que la variable pertenezca a una distribución normal, porque el gráfico
está diseñado para desglosar la estructura de los datos en la escala de la 
variable. Siguiendo nuestro ejemplo, queremos conocer el Hacinamiento por rangos
de porcentaje de Población Indígena. Para ello, clasificaremos la variable `P_Indi`
en 5 clases de igual intervalo. Luego haremos un box plot por cada rango.

```{r boxplot}
variablesshp %>%
  pivot_longer(cols = c(P_Indi, P_Hacina),
               names_to = "variable",
               values_to = "Percent") %>%
ggplot(aes(y = Percent)) +
  geom_boxplot() +
  facet_grid(~variable, scales = "free_x")
```

```{r boxplot}
variablesshp %>%
  mutate(Rangos_Indigena = cut_interval(P_Indi, 5)) %>%
  ggplot(aes(x = Rangos_Indigena, y = P_Hacina)) +
  geom_boxplot()
```

El resultado muestra que a mayor porcentaje de población indígena, las personas 
viven en más hacinamiento. Aunque este comportamiento es a nivel general, también
se puede encontrar que los sectores con menor población indígena puede no ser tan
evidente que vivan en hacinamiento. Es decir, también la población no indígena
puede estar en hacinamiento.


## Polígonos de Voronoi

El último análisis será usado para relacionar las variables con el espacio de una
forma muy sencilla. Los polígonos de Voronoi, o Diagrama de Voronoi. Permite 
visualizar el área de influencia de las entidades basado en la premisa de cercanía.
Esto quiere decir que la distancia máxima de influencia entre dos entidades será
igual a la mitad de la distancia entre ellas. Veamos con el ejemplo.

Primero vamos a cargar la capa *"Sectores_censales_variables_centroids.shp"* desde
la carpeta de datos. Para eso usaremos la función `sf::st_read()`. Luego calcularemos
el voronoi correspondiente para cada entidad de la capa cargada.

```{r vorinoi_data}
# Cargar la capa Sectores_censales_variables_centroids
variablesshpcentroids <- sf::st_read("../Datos/Sectores_censales_variables_centroids.shp")

# Voronoi con paque sf y ggplot
g <- sf::st_geometry(variablesshpcentroids)
bb <- sf::st_as_sfc(st_bbox(variablesshpcentroids))

voronoi_sf <- st_voronoi(do.call(c, g), st_geometry(bb)) %>%
  st_collection_extract() %>%
  st_sfc(crs = st_crs(variablesshpcentroids)) %>% 
  st_sf() %>%
  st_join(variablesshpcentroids)
```

Vemos que la capa original, es una capa de puntos de dimensiones XY en el SRC 
EPSG:32717 (WGS84 UTM 17S). Por lo tanto las unidades serán en metros tanto para
coordenadas como para la distancia. El resultado del calculo de los Voronoi es una
capa de polígonos de una extensión mucho más grande que la capa original, debido 
a que la convergencia de las líneas de las teselas puede extenderse al infinito.

```{r, vorinoi_plot}
ggplot(voronoi_sf) +
  geom_sf(aes(fill = P_Hacina), size = 0.25) +
  #geom_sf(data = variablesshp, fill = NA, size = 0.25, color = "grey50") +
  #geom_sf(data = variablesshpcentroids, aes(size = P_Indi)) +
  scale_fill_gradientn(colors = RColorBrewer::brewer.pal(9, "GnBu")) +
  scale_color_gradientn(colors = RColorBrewer::brewer.pal(9, "GnBu")) +
  #scale_size_continuous(range = c(0.5,3))+
  coord_sf(xlim = range(st_coordinates(bb)[,1]),
           ylim = range(st_coordinates(bb)[,2]))
```

Los colores de los polígonos de Voronoi se intensifican de acuerdo con el 
porcentaje de hacinamiento que hay en ese sector censal. Este ejercicio, es para
visualizar información gráfica a partir de puntos, sin embargo también puede servir
para hacer una regionalización rápida de valores para cada área de influencia del
las entidades presentes en la capa.


